/* pcu_l1_if.cpp
 *
 * Copyright (C) 2012 Andreas Eversberg <jolly@eversberg.eu>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <inttypes.h>
extern "C" {
#include <osmocom/core/talloc.h>
#include <osmocom/core/select.h>
#include <osmocom/core/msgb.h>
#include <osmocom/core/gsmtap_util.h>
#include <osmocom/core/gsmtap.h>
}

#include <gprs_rlcmac.h>
#include <pcu_l1_if.h>
#include <gprs_debug.h>
#include <gprs_bssgp_pcu.h>
#include <osmocom/pcu/pcuif_proto.h>
#include <bts.h>
#include <tbf.h>

// FIXME: move this, when changed from c++ to c.
extern "C" {
void *l1if_open_pdch(uint8_t trx_no, uint32_t hlayer1,
		     struct gsmtap_inst *gsmtap);
int l1if_connect_pdch(void *obj, uint8_t ts);
int l1if_pdch_req(void *obj, uint8_t ts, int is_ptcch, uint32_t fn,
        uint16_t arfcn, uint8_t block_nr, uint8_t *data, uint8_t len);
}

extern void *tall_pcu_ctx;
struct pcu_fail_evt_rep_sig_data alarm_sig_data;

/* Allows to pack performance counter into data buffer at specified offset for a request cmd */
/* data: data buffer where to pack the information */
/* id: item id that is packed into the data buffer */
/* counter value to pack into the data buffer */
/* offset: offset in the buffer where to pack the information */
/* outputs a trace only if GPRS_STATS_COUNTER_INFO is defined in gprs_bssgp_pcu.h */
#define PCU_PACK_PCOUNTER(data, id, counter, offset) \
{\
	(data)[(offset) + 0] = (id);\
	(data)[(offset) + 1] = 0;\
	(data)[(offset) + 2] = 4;\
	(data)[(offset) + 3] = *(((uint8_t *)&(counter)) + 3);\
	(data)[(offset) + 4] = *(((uint8_t *)&(counter)) + 2);\
	(data)[(offset) + 5] = *(((uint8_t *)&(counter)) + 1);\
	(data)[(offset) + 6] = *(((uint8_t *)&(counter)) + 0);\
	LOGP(DPCU, LOGL_DEBUG,"****!Stats request: " #counter ", counter: %u!****\n", (counter));\
}

static void update_pcu_param(struct gprs_rlcmac_bts *bts, uint8_t initial_mcs, uint8_t oml_max_mcs,
		uint8_t initial_cs, uint8_t oml_max_cs) {

	/* use max CS value instructed by the BTS */
	if ((oml_max_cs < 1) || (oml_max_cs > 4))
		/* use CS-4 as default */
		bts->max_cs_dl = bts->max_cs_ul = 4;
	else
		bts->max_cs_dl = bts->max_cs_ul = oml_max_cs;

	/* validate initial CS instructed by the BTS */
	if (initial_cs < 1)
		bts->initial_cs_dl = bts->initial_cs_ul = 1;
	else if (initial_cs > 4)
		bts->initial_cs_dl = bts->initial_cs_ul = 4;
	else if (initial_cs > oml_max_mcs)
		bts->initial_cs_dl = bts->initial_cs_ul = oml_max_cs;
	else
		bts->initial_cs_dl = bts->initial_cs_ul = initial_cs;

	/* use max MCS value instructed by the BTS */
	if ((oml_max_mcs < 1) || (oml_max_mcs > 9))
		/* use MCS-9 as default */
		bts->max_mcs_dl = bts->max_mcs_ul = 9;
	else
		bts->max_mcs_dl = bts->max_mcs_ul = oml_max_mcs;

	/* validate initial MCS instructed by the BTS */
	if (initial_mcs < 1)
		bts->initial_mcs_dl = bts->initial_mcs_ul = 1;
	else if (initial_mcs > 9)
		bts->initial_mcs_dl = bts->initial_mcs_ul = 9;
	else if (initial_mcs > oml_max_mcs)
		bts->initial_mcs_dl = bts->initial_mcs_ul = oml_max_mcs;
	else
		bts->initial_mcs_dl = bts->initial_mcs_ul = initial_mcs;

	if (!bts->egprs_enabled) {
		/* force allocation algorithm B if it is not defined in configuration file */
		if (bts->alloc_algorithm == alloc_algorithm_dynamic)
			bts->alloc_algorithm = alloc_algorithm_b;
		return;
	}

	/* set windows size parameter if it is not defined in configuration file */
	if (bts->ws_pdch == 0)
		bts->ws_pdch = 104;

	/* disable CoDEL if it is not defined in configuration file */
	if (bts->llc_codel_interval_msec == LLC_CODEL_USE_DEFAULT)
		bts->llc_codel_interval_msec = LLC_CODEL_DISABLE;

	/* force allocation algorithm B if it is not defined in configuration file */
	if (bts->alloc_algorithm == alloc_algorithm_dynamic)
		bts->alloc_algorithm = alloc_algorithm_b;

	/* force infinitive LLC queue lifetime if it is not defined in configuration file */
	if (!bts->force_llc_lifetime)
		bts->force_llc_lifetime = 0xffff;

	/* force flow control BVC bucket size */
	if (!bts->fc_bvc_bucket_size)
		bts->fc_bvc_bucket_size = 596000;

	/* force flow control BVC leak rate */
	if (!bts->fc_bvc_leak_rate)
		bts->fc_bvc_leak_rate = 596000;

	/* force flow control MS bucket size */
	if (!bts->fc_ms_bucket_size)
		bts->fc_ms_bucket_size = 596000;

	/* force flow control MS leak rate */
	if (!bts->fc_ms_leak_rate)
		bts->fc_ms_leak_rate = 596000;

	return;
}
/*
 * PCU messages
 */

struct msgb *pcu_msgb_alloc(uint8_t msg_type, uint8_t bts_nr)
{
	struct msgb *msg;
	struct gsm_pcu_if *pcu_prim;

	msg = msgb_alloc(sizeof(struct gsm_pcu_if), "pcu_sock_tx");
	if (!msg)
		return NULL;
	msgb_put(msg, sizeof(struct gsm_pcu_if));
	pcu_prim = (struct gsm_pcu_if *) msg->data;
	pcu_prim->msg_type = msg_type;
	pcu_prim->bts_nr = bts_nr;

	return msg;
}

const struct value_string gsm_pcu_if_text_type_names[] = {
	OSMO_VALUE_STRING(PCU_VERSION),
	OSMO_VALUE_STRING(PCU_OML_ALERT),
	{ 0, NULL }
};

int pcu_tx_txt_ind(enum gsm_pcu_if_text_type t, const char *fmt, ...)
{
	struct gsm_pcu_if *pcu_prim;
	struct gsm_pcu_if_txt_ind *txt;
	va_list ap;
	char *rep;
	struct msgb *msg = pcu_msgb_alloc(PCU_IF_MSG_TXT_IND, 0);
	if (!msg)
		return -ENOMEM;

	pcu_prim = (struct gsm_pcu_if *) msg->data;
	txt = &pcu_prim->u.txt_ind;
	txt->type = t;

	va_start(ap, fmt);
	rep = talloc_vasprintf(tall_pcu_ctx, fmt, ap);
	va_end(ap);

	if (!rep)
		return -ENOMEM;

	osmo_strlcpy(txt->text, rep, TXT_MAX_LEN);
	talloc_free(rep);

	LOGP(DL1IF, LOGL_INFO, "Sending %s TXT as %s to BTS\n", txt->text,
		get_value_string(gsm_pcu_if_text_type_names, t));

	return pcu_sock_send(msg);
}

static int pcu_tx_act_req(uint8_t trx, uint8_t ts, uint8_t activate)
{
	struct msgb *msg;
	struct gsm_pcu_if *pcu_prim;
	struct gsm_pcu_if_act_req *act_req;

	LOGP(DL1IF, LOGL_INFO, "Sending %s request: trx=%d ts=%d\n",
		(activate) ? "activate" : "deactivate", trx, ts);

	msg = pcu_msgb_alloc(PCU_IF_MSG_ACT_REQ, 0);
	if (!msg)
		return -ENOMEM;
	pcu_prim = (struct gsm_pcu_if *) msg->data;
	act_req = &pcu_prim->u.act_req;
	act_req->activate = activate;
	act_req->trx_nr = trx;
	act_req->ts_nr = ts;

	return pcu_sock_send(msg);
}

static int pcu_tx_data_req(uint8_t trx, uint8_t ts, uint8_t sapi,
	uint16_t arfcn, uint32_t fn, uint8_t block_nr, uint8_t *data,
	uint8_t len)
{
	struct msgb *msg;
	struct gsm_pcu_if *pcu_prim;
	struct gsm_pcu_if_data *data_req;

	LOGP(DL1IF, LOGL_DEBUG, "Sending data request: trx=%d ts=%d sapi=%d "
		"arfcn=%d fn=%d block=%d data=%s\n", trx, ts, sapi, arfcn, fn,
		block_nr, osmo_hexdump(data, len));

	msg = pcu_msgb_alloc(PCU_IF_MSG_DATA_REQ, 0);
	if (!msg)
		return -ENOMEM;
	pcu_prim = (struct gsm_pcu_if *) msg->data;
	data_req = &pcu_prim->u.data_req;

	data_req->sapi = sapi;
	data_req->fn = fn;
	data_req->arfcn = arfcn;
	data_req->trx_nr = trx;
	data_req->ts_nr = ts;
	data_req->block_nr = block_nr;
	memcpy(data_req->data, data, len);
	data_req->len = len;

	return pcu_sock_send(msg);
}

int pcu_tx_nm_fail_evt(uint8_t event_type, uint8_t event_severity,
	uint8_t cause_type, uint16_t event_cause, const char *add_text)
{
	struct msgb *msg;
	struct gsm_pcu_if *pcu_prim;

	msg = pcu_msgb_alloc(PCU_IF_MSG_FAILURE_EVT_IND, 0);
	if (!msg)
		return -ENOMEM;

	pcu_prim = (struct gsm_pcu_if *) msg->data;
	pcu_prim->u.failure_evt_ind.event_type = event_type;
	pcu_prim->u.failure_evt_ind.event_severity = event_severity;
	pcu_prim->u.failure_evt_ind.event_cause = event_cause;
	pcu_prim->u.failure_evt_ind.cause_type = cause_type;
	strcpy(pcu_prim->u.failure_evt_ind.add_text, add_text);

	LOGP(DL1IF, LOGL_DEBUG, "[PCU->BTS] Sending FAILure EVT REP dump %s (%d)\n", osmo_hexdump(msg->data , msg->data_len), msg->data_len);
	return pcu_sock_send(msg);
}

void pcu_l1if_tx_pdtch(msgb *msg, uint8_t trx, uint8_t ts, uint16_t arfcn,
	uint32_t fn, uint8_t block_nr)
{
#ifdef ENABLE_DIRECT_PHY
	struct gprs_rlcmac_bts *bts = bts_main_data();

	if (bts->trx[trx].fl1h) {
		l1if_pdch_req(bts->trx[trx].fl1h, ts, 0, fn, arfcn, block_nr,
			msg->data, msg->len);
		msgb_free(msg);
		return;
	}
#endif
	pcu_tx_data_req(trx, ts, PCU_IF_SAPI_PDTCH, arfcn, fn, block_nr,
			msg->data, msg->len);
	msgb_free(msg);
}

void pcu_l1if_tx_ptcch(msgb *msg, uint8_t trx, uint8_t ts, uint16_t arfcn,
	uint32_t fn, uint8_t block_nr)
{
	struct gprs_rlcmac_bts *bts = bts_main_data();

	if (bts->gsmtap_categ_mask & (1 << PCU_GSMTAP_C_DL_PTCCH))
		gsmtap_send(bts->gsmtap, arfcn, ts, GSMTAP_CHANNEL_PTCCH, 0, fn, 0, 0, msg->data, msg->len);
#ifdef ENABLE_DIRECT_PHY
	if (bts->trx[trx].fl1h) {
		l1if_pdch_req(bts->trx[trx].fl1h, ts, 1, fn, arfcn, block_nr,
			msg->data, msg->len);
		msgb_free(msg);
		return;
	}
#endif
	pcu_tx_data_req(trx, ts, PCU_IF_SAPI_PTCCH, arfcn, fn, block_nr,
			msg->data, msg->len);
	msgb_free(msg);
}

void pcu_l1if_tx_agch(bitvec * block, int plen)
{
	uint8_t data[23]; /* prefix PLEN */
	
	/* FIXME: why does OpenBTS has no PLEN and no fill in message? */
	bitvec_pack(block, data + 1);
	data[0] = (plen << 2) | 0x01;
	pcu_tx_data_req(0, 0, PCU_IF_SAPI_AGCH, 0, 0, 0, data, 23);
}

void pcu_l1if_tx_pch(bitvec * block, int plen, const char *imsi)
{
	uint8_t data[23 + 3]; /* prefix PLEN */

	/* paging group */
	if (!imsi || strlen(imsi) < 3)
		return;
	imsi += strlen(imsi) - 3;
	/* store last 3 IMSI digits in order to calculate (IMSI%1000) in PAGING GROUP calculation at BTS side */
	/* PAGING_GROUP(0..N-1) = [(IMSI % 1000) mod (BS_CC_CHANS * N)] mod N */
	/* PAGING_BLOCK_INDEX = PAGING_GROUP mod (N/BS_PA_MFRMS) */
	data[0] = imsi[0];
	data[1] = imsi[1];
	data[2] = imsi[2];

	bitvec_pack(block, data + 3 + 1);
	data[3] = (plen << 2) | 0x01;	/* L2 pseudo length, TS 04.18, section 10.5.2.19 */

	pcu_tx_data_req(0, 0, PCU_IF_SAPI_PCH, 0, 0, 0, data, 23 + 3);
}

extern "C" void pcu_rx_block_time(uint16_t arfcn, uint32_t fn, uint8_t ts_no)
{
	BTS::main_bts()->set_current_block_frame_number(fn, 0);
}

extern "C" void pcu_rx_ra_time(uint16_t arfcn, uint32_t fn, uint8_t ts_no)
{
	/* access bursts may arrive some bursts earlier */
	BTS::main_bts()->set_current_block_frame_number(fn, 5);
}

extern "C" int pcu_rx_ra_ind_pdtch(uint8_t trx, uint8_t ts, uint32_t fn, uint8_t ta)
{
	struct gprs_rlcmac_pdch *pdch;

	pdch = &bts_main_data()->trx[trx].pdch[ts];

	if (!pdch) {
		LOGP(DRLCMAC, LOGL_DEBUG, "Failed to get PDCH struct for PRACH TA = %u on TRX = %d, TS = %d, FN = %d\n",
				ta, trx, ts, fn);
		return -EINVAL;
	}

	return pdch->rcv_control_ack(trx, ts, ta, fn);
}

extern "C" int pcu_rx_data_ind_pdtch(uint8_t trx_no, uint8_t ts_no, uint8_t *data,
	uint8_t len, uint32_t fn, struct pcu_l1_meas *meas)
{
	struct gprs_rlcmac_pdch *pdch;

	pdch = &bts_main_data()->trx[trx_no].pdch[ts_no];
	return pdch->rcv_block(data, len, fn, meas);
}

static int pcu_rx_data_ind_bcch(uint8_t *data, uint8_t len)
{
	struct gprs_rlcmac_bts *bts = bts_main_data();
	struct gsm48_si13_info si13_info;

	if (len == 0) {
		bts->si13_is_set = false;
		LOGP(DL1IF, LOGL_INFO, "Received PCU data indication with empty SI13: cache cleaned\n");
		return 0;
	}

	if (len != GSM_MACBLOCK_LEN) {
		LOGP(DL1IF, LOGL_ERROR, "Received PCU data indication with SI13 with unexpected length %u\n", len);
		return -EINVAL;
	}

	memcpy(bts->si13, data, GSM_MACBLOCK_LEN);
	bts->si13_is_set = true;
	/* TODO: decode SI13 here */
	LOGP(DL1IF, LOGL_DEBUG, "[BTS->PCU ] BCCH data indication with SI13 buffer: %s\n", osmo_hexdump(data, GSM_MACBLOCK_LEN));

	/* override GPRS/EDGE parameters */
	if (!bts->force_cs) {
		/* parsing S113 */
		si13_info = bts->bts->bts_si13_parser(bts->si13);

		LOGP(DL1IF, LOGL_NOTICE, "[BTS->PCU ] Update PCU operation mode: %s\n", si13_info.cell_opts.ext_info.egprs_supported? "EGPRS" : "GPRS");
		bts->egprs_enabled = si13_info.cell_opts.ext_info.egprs_supported;

		LOGP(DL1IF, LOGL_NOTICE, "[BTS->PCU ] Update CONTROL_ACK_TYPE: %s\n", si13_info.cell_opts.ctrl_ack_type_use_block? "RLC/MAC" : "4AB");
		bts->ctrl_ack_type = si13_info.cell_opts.ctrl_ack_type_use_block;

		LOGP(DL1IF, LOGL_NOTICE, "[BTS->PCU ] Update GPRS PC ALPHA: %d\n", si13_info.pwr_ctrl_param.alpha);
		bts->alpha = si13_info.pwr_ctrl_param.alpha;

		update_pcu_param(bts, bts->initial_mcs_ul, bts->max_mcs_ul, bts->initial_cs_ul, bts->max_cs_ul);
	}

	/* TODO: there is no MS power control implementation in the PCU, thus updating PC_ALPHA value does not affect the PCU operation */
	return 0;
}

static int pcu_rx_data_ind(struct gsm_pcu_if_data *data_ind)
{
	struct gprs_rlcmac_bts *bts = bts_main_data();
	int rc;
	pcu_l1_meas meas;

	LOGP(DL1IF, LOGL_DEBUG, "Data indication received: sapi=%d arfcn=%d "
		"block=%d data=%s\n", data_ind->sapi,
		data_ind->arfcn, data_ind->block_nr,
		osmo_hexdump(data_ind->data, data_ind->len));

	switch (data_ind->sapi) {
	case PCU_IF_SAPI_PDTCH:
		meas.set_rssi(data_ind->rssi);
#ifndef ENABLE_DIRECT_PHY
		/* convert BER to % value */
		meas.set_ber(data_ind->ber10k / 100);
		meas.set_bto(data_ind->ta_offs_qbits);
		meas.set_link_qual(data_ind->lqual_cb / 10);
		LOGP(DL1IF, LOGL_DEBUG, "Data indication with raw measurements received: BER10k = %d, BTO = %d, Q = %d\n",
		     data_ind->ber10k, data_ind->ta_offs_qbits, data_ind->lqual_cb);
#endif
		rc = pcu_rx_data_ind_pdtch(data_ind->trx_nr, data_ind->ts_nr,
			data_ind->data, data_ind->len, data_ind->fn,
			&meas);
		break;
	case PCU_IF_SAPI_BCCH:
		rc = pcu_rx_data_ind_bcch(data_ind->data, data_ind->len);
		break;
	default:
		LOGP(DL1IF, LOGL_ERROR, "Received PCU data indication with "
			"unsupported sapi %d\n", data_ind->sapi);
		rc = -EINVAL;
	}

	if (rc < 0 && (bts->gsmtap_categ_mask & (1 <<PCU_GSMTAP_C_UL_UNKNOWN))) {
		gsmtap_send(bts->gsmtap, data_ind->arfcn | GSMTAP_ARFCN_F_UPLINK, data_ind->ts_nr,
			    GSMTAP_CHANNEL_PACCH, 0, data_ind->fn, 0, 0, data_ind->data, data_ind->len);
	}

	return rc;
}

static int pcu_rx_data_cnf(struct gsm_pcu_if_data *data_cnf)
{
	int rc = 0;

	LOGP(DL1IF, LOGL_DEBUG, "Data confirm received: sapi=%d fn=%d\n",
		data_cnf->sapi, data_cnf->fn);

	switch (data_cnf->sapi) {
	case PCU_IF_SAPI_PCH:
		if (data_cnf->data[2] == 0x3f)
			BTS::main_bts()->rcv_imm_ass_cnf(data_cnf->data, data_cnf->fn);
		break;
	default:
		LOGP(DL1IF, LOGL_ERROR, "Received PCU data confirm with "
			"unsupported sapi %d\n", data_cnf->sapi);
		rc = -EINVAL;
	}

	return rc;
}

// FIXME: remove this, when changed from c++ to c.
extern "C" int pcu_rx_rts_req_pdtch(uint8_t trx, uint8_t ts,
	uint32_t fn, uint8_t block_nr)
{
	return gprs_rlcmac_rcv_rts_block(bts_main_data(),
					trx, ts, fn, block_nr);
}

static int pcu_rx_rts_req(struct gsm_pcu_if_rts_req *rts_req)
{
	int rc = 0;

	LOGP(DL1IF, LOGL_DEBUG, "RTS request received: trx=%d ts=%d sapi=%d "
		"arfcn=%d fn=%d block=%d\n", rts_req->trx_nr, rts_req->ts_nr,
		rts_req->sapi, rts_req->arfcn, rts_req->fn, rts_req->block_nr);

	switch (rts_req->sapi) {
	case PCU_IF_SAPI_PDTCH:
		pcu_rx_rts_req_pdtch(rts_req->trx_nr, rts_req->ts_nr,
			rts_req->fn, rts_req->block_nr);
		break;
	case PCU_IF_SAPI_PTCCH:
		/* FIXME */
		{
			struct msgb *msg = msgb_alloc(23, "l1_prim");
			memset(msgb_put(msg, 23), 0x2b, 23);
			pcu_l1if_tx_ptcch(msg, rts_req->trx_nr, rts_req->ts_nr,
				rts_req->arfcn, rts_req->fn, rts_req->block_nr);
		}
		break;
	default:
		LOGP(DL1IF, LOGL_ERROR, "Received PCU RTS request with "
			"unsupported sapi %d\n", rts_req->sapi);
		rc = -EINVAL;
	}

	return rc;
}

static int pcu_rx_rach_ind(struct gsm_pcu_if_rach_ind *rach_ind)
{
	int rc = 0;

	LOGP(DL1IF, LOGL_INFO, "RACH request received: sapi=%d "
		"qta=%d, ra=%d, fn=%d\n", rach_ind->sapi, rach_ind->qta,
		rach_ind->ra, rach_ind->fn);

	switch (rach_ind->sapi) {
	case PCU_IF_SAPI_RACH:
		rc = BTS::main_bts()->rcv_rach(
			rach_ind->ra, rach_ind->fn,
			rach_ind->qta, rach_ind->is_11bit,
			(ph_burst_type)rach_ind->burst_type);
		break;
	default:
		LOGP(DL1IF, LOGL_ERROR, "Received PCU rach request with "
			"unsupported sapi %d\n", rach_ind->sapi);
		rc = -EINVAL;
	}

	return rc;
}

static int pcu_rx_info_ind(struct gsm_pcu_if_info_ind *info_ind)
{
	struct gprs_rlcmac_bts *bts = bts_main_data();
	struct gprs_bssgp_pcu *pcu;
	struct gprs_rlcmac_pdch *pdch;
	struct in_addr ia;
	int rc = 0;
	unsigned int trx, ts;
	int i;
	struct pcu_alarm_list *alarm_list;
	uint8_t oml_max_cs = 1, oml_max_mcs = 1;
	uint16_t cell_id = ntohs(info_ind->cell_id);

	if (info_ind->version != PCU_IF_VERSION) {
		fprintf(stderr, "PCU interface version number of BTS (%d) is "
			"different (%d).\nPlease re-compile!\n",
			info_ind->version, PCU_IF_VERSION);

		memcpy(alarm_sig_data.spare, &info_ind->version, sizeof(unsigned int));
		osmo_signal_dispatch(SS_L_GLOBAL, S_PCU_NM_WRONG_IF_VER_ALARM, &alarm_sig_data);

		/* allocate new list of sent alarms */
		alarm_list = talloc_zero(tall_pcu_ctx, struct pcu_alarm_list);
		if (!alarm_list)
			return -EIO;

		alarm_list->alarm_signal = S_PCU_NM_WRONG_IF_VER_ALARM;
		/* add alarm to sent list */
		llist_add(&alarm_list->list, &bts->alarm_list);
		LOGP(DL1IF, LOGL_DEBUG, "PCU alarm 0x%04x added to sent alarm list\n", alarm_list->alarm_signal);
		return -EPERM;
	}

	LOGP(DL1IF, LOGL_DEBUG, "Info indication received:\n");

	if (!(info_ind->flags & PCU_IF_FLAG_ACTIVE)) {
		LOGP(DL1IF, LOGL_NOTICE, "BTS not available\n");
		if (!bts->active)
			return -EAGAIN;
bssgp_failed:
		bts->active = false;
		/* free all TBF */
		for (trx = 0; trx < ARRAY_SIZE(bts->trx); trx++) {
			bts->trx[trx].arfcn = info_ind->trx[trx].arfcn;
			for (ts = 0; ts < ARRAY_SIZE(bts->trx[0].pdch); ts++)
				bts->trx[trx].pdch[ts].free_resources();
		}
		gprs_bssgp_destroy();
		exit(0);
	}
	LOGP(DL1IF, LOGL_INFO, "BTS available\n");
	LOGP(DL1IF, LOGL_DEBUG, " mcc=%03u\n", info_ind->mcc);
	LOGP(DL1IF, LOGL_DEBUG, " mnc=%0*u\n", info_ind->mnc_3_digits, info_ind->mnc);
	LOGP(DL1IF, LOGL_DEBUG, " lac=%d\n", info_ind->lac);
	LOGP(DL1IF, LOGL_DEBUG, " rac=%d\n", info_ind->rac);
	LOGP(DL1IF, LOGL_DEBUG, " cell_id=%d\n", cell_id);
	LOGP(DL1IF, LOGL_DEBUG, " bsic=%d\n", info_ind->bsic);
	LOGP(DL1IF, LOGL_DEBUG, " nsei=%d\n", info_ind->nsei);
	LOGP(DL1IF, LOGL_DEBUG, " nse_timer=%d %d %d %d %d %d %d\n",
		info_ind->nse_timer[0], info_ind->nse_timer[1],
		info_ind->nse_timer[2], info_ind->nse_timer[3],
		info_ind->nse_timer[4], info_ind->nse_timer[5],
		info_ind->nse_timer[6]);
	LOGP(DL1IF, LOGL_DEBUG, " cell_timer=%d %d %d %d %d %d %d %d %d %d "
		"%d\n",
		info_ind->cell_timer[0], info_ind->cell_timer[1],
		info_ind->cell_timer[2], info_ind->cell_timer[3],
		info_ind->cell_timer[4], info_ind->cell_timer[5],
		info_ind->cell_timer[6], info_ind->cell_timer[7],
		info_ind->cell_timer[8], info_ind->cell_timer[9],
		info_ind->cell_timer[10]);
	LOGP(DL1IF, LOGL_DEBUG, " repeat_time=%d\n", info_ind->repeat_time);
	LOGP(DL1IF, LOGL_DEBUG, " repeat_count=%d\n", info_ind->repeat_count);
	LOGP(DL1IF, LOGL_DEBUG, " bvci=%d\n", info_ind->bvci);
	LOGP(DL1IF, LOGL_DEBUG, " t3142=%d\n", info_ind->t3142);
	LOGP(DL1IF, LOGL_DEBUG, " t3169=%d\n", info_ind->t3169);
	LOGP(DL1IF, LOGL_DEBUG, " t3191=%d\n", info_ind->t3191);
	LOGP(DL1IF, LOGL_DEBUG, " t3193=%d (ms)\n", info_ind->t3193_10ms * 10);
	LOGP(DL1IF, LOGL_DEBUG, " t3195=%d\n", info_ind->t3195);
	LOGP(DL1IF, LOGL_DEBUG, " n3101=%d\n", info_ind->n3101);
	LOGP(DL1IF, LOGL_DEBUG, " n3103=%d\n", info_ind->n3103);
	LOGP(DL1IF, LOGL_DEBUG, " n3105=%d\n", info_ind->n3105);
	LOGP(DL1IF, LOGL_DEBUG, " cv_countdown=%d\n", info_ind->cv_countdown);
	LOGP(DL1IF, LOGL_DEBUG, " dl_tbf_ext=%d\n", info_ind->dl_tbf_ext);
	LOGP(DL1IF, LOGL_DEBUG, " ul_tbf_ext=%d\n", info_ind->ul_tbf_ext);
	bts->bsic = info_ind->bsic;
	for (i = 0; i < 4; i++) {
		if ((info_ind->flags & (PCU_IF_FLAG_CS1 << i))) {
			LOGP(DL1IF, LOGL_DEBUG, " Use CS%d\n", i+1);
			oml_max_cs = i + 1;
		}
	}

	for (i = 0; i < 9; i++) {
		if ((info_ind->flags & (PCU_IF_FLAG_MCS1 << i))) {
			LOGP(DL1IF, LOGL_DEBUG, " Use MCS%d\n", i+1);
			oml_max_mcs = i + 1;
		}
	}

	LOGP(DL1IF, LOGL_DEBUG, " initial_cs=%d\n", info_ind->initial_cs);
	LOGP(DL1IF, LOGL_DEBUG, " initial_mcs=%d\n", info_ind->initial_mcs);
	LOGP(DL1IF, LOGL_DEBUG, " nsvci=%d\n", info_ind->nsvci[0]);
	LOGP(DL1IF, LOGL_DEBUG, " local_port=%d\n", info_ind->local_port[0]);
	LOGP(DL1IF, LOGL_DEBUG, " remote_port=%d\n", info_ind->remote_port[0]);
	ia.s_addr = htonl(info_ind->remote_ip[0]);
	LOGP(DL1IF, LOGL_DEBUG, " remote_ip=%s\n", inet_ntoa(ia));

	pcu = gprs_bssgp_create_and_connect(bts, info_ind->local_port[0],
		info_ind->remote_ip[0], info_ind->remote_port[0],
		info_ind->nsei, info_ind->nsvci[0], info_ind->bvci,
		info_ind->mcc, info_ind->mnc, info_ind->mnc_3_digits, info_ind->lac, info_ind->rac,
		cell_id);
	if (!pcu) {
		LOGP(DL1IF, LOGL_NOTICE, "SGSN not available\n");
		goto bssgp_failed;
	}

	bts->cs1 = !!(info_ind->flags & PCU_IF_FLAG_CS1);
	bts->cs2 = !!(info_ind->flags & PCU_IF_FLAG_CS2);
	bts->cs3 = !!(info_ind->flags & PCU_IF_FLAG_CS3);
	bts->cs4 = !!(info_ind->flags & PCU_IF_FLAG_CS4);
	if (!bts->cs1 && !bts->cs2 && !bts->cs3 && !bts->cs4)
		bts->cs1 = 1;

	if (info_ind->t3142) { /* if timer values are set */
		bts->t3142 = info_ind->t3142;
		bts->t3169 = info_ind->t3169;
		bts->t3191 = info_ind->t3191;
		bts->t3193_msec = info_ind->t3193_10ms * 10;
		bts->t3195 = info_ind->t3195;
		bts->n3101 = info_ind->n3101;
		bts->n3103 = info_ind->n3103;
		bts->n3105 = info_ind->n3105;
	}
	/* override GPRS/EDGE parameters using via BTS OML */
	if (!bts->force_cs)
		update_pcu_param(bts, info_ind->initial_mcs, oml_max_mcs, info_ind->initial_cs, oml_max_cs);

	LOGP(DL1IF, LOGL_DEBUG, "PCU is running in %s mode, max_dl_%scs=%u, initial_dl_%scs=%u, max_ul_%scs=%u, initial_ul_%scs=%u\n",
			bts->egprs_enabled ? "EGDE" : "GPRS",
			bts->egprs_enabled ? "m" : "",
			bts->egprs_enabled ? bts->max_mcs_dl : bts->max_cs_dl,
			bts->egprs_enabled ? "m" : "",
			bts->egprs_enabled ? bts->initial_mcs_dl : bts->initial_cs_dl,
			bts->egprs_enabled ? "m" : "",
			bts->egprs_enabled ? bts->max_mcs_ul : bts->max_cs_ul,
			bts->egprs_enabled ? "m" : "",
			bts->egprs_enabled ? bts->initial_mcs_ul : bts->initial_cs_ul);

	for (trx = 0; trx < ARRAY_SIZE(bts->trx); trx++) {
		bts->trx[trx].arfcn = info_ind->trx[trx].arfcn;
		if ((info_ind->flags & PCU_IF_FLAG_SYSMO)
		 && info_ind->trx[trx].hlayer1) {
#ifdef ENABLE_DIRECT_PHY
			LOGP(DL1IF, LOGL_DEBUG, " TRX %d hlayer1=%x\n", trx,
				info_ind->trx[trx].hlayer1);
				if (!bts->trx[trx].fl1h)
					bts->trx[trx].fl1h = l1if_open_pdch(
						trx,
						info_ind->trx[trx].hlayer1,
						bts->gsmtap);
			if (!bts->trx[trx].fl1h) {
				LOGP(DL1IF, LOGL_FATAL, "Failed to open direct "
					"DSP access for PDCH.\n");

				alarm_sig_data.spare[0] = trx;
				osmo_signal_dispatch(SS_L_GLOBAL, S_PCU_NM_FAIL_OPEN_PDCH_ALARM, &alarm_sig_data);
				exit(0);
			}
#else
			LOGP(DL1IF, LOGL_FATAL, "Compiled without direct DSP "
					"access for PDCH, but enabled at "
					"BTS. Please deactivate it!\n");
			exit(0);
#endif
		}

		for (ts = 0; ts < ARRAY_SIZE(bts->trx[0].pdch); ts++) {
			pdch = &bts->trx[trx].pdch[ts];
			if ((info_ind->trx[trx].pdch_mask & (1 << ts))) {
				/* FIXME: activate dynamically at RLCMAC */
				if (!pdch->is_enabled()) {
					pcu_tx_act_req(trx, ts, 1);
					pdch->enable();
				}
				pdch->tsc = info_ind->trx[trx].tsc[ts];
				LOGP(DL1IF, LOGL_INFO, "ACTIVATE PDCH: trx=%d ts=%d\n",
					trx, ts);
			} else {
				if (pdch->is_enabled()) {
					/* Needs help from upper layer to reallocate PDCH resource used in this TRX */
					if (bts->dyn_pdch_reconf_enabled) {
						if (pdch->reallocate_resources() < 0) {
							pcu_tx_act_req(trx, ts, 0);
							pdch->free_resources();
							pdch->disable();
						}
					} else {
						pcu_tx_act_req(trx, ts, 0);
						pdch->free_resources();
						pdch->disable();
					}
				}
			}
		}
	}

	bts->active = true;
	return rc;
}

static int pcu_rx_time_ind(struct gsm_pcu_if_time_ind *time_ind)
{
	uint8_t fn13 = time_ind->fn % 13;

	/* omit frame numbers not starting at a MAC block */
	if (fn13 != 0 && fn13 != 4 && fn13 != 8)
		return 0;

	LOGP(DL1IF, LOGL_DEBUG, "Time indication received: %d\n", time_ind->fn % 52);

	BTS::main_bts()->set_current_frame_number(time_ind->fn);
	return 0;
}

static int pcu_rx_pag_req(struct gsm_pcu_if_pag_req *pag_req)
{
	LOGP(DL1IF, LOGL_DEBUG, "Paging request received: chan_needed=%d "
		"length=%d\n", pag_req->chan_needed, pag_req->identity_lv[0]);

	return BTS::main_bts()->add_paging(pag_req->chan_needed,
						pag_req->identity_lv);
}

int pcu_tx_nm_start_meas_ack_nack(struct gsm_pcu_if_start_stop_meas_req *start_meas_req, uint8_t nack_cause)
{
	struct msgb *msg;
	uint8_t msg_type;
	struct gsm_pcu_if *pcu_prim;

	if (nack_cause)
		msg_type = PCU_IF_MSG_START_MEAS_NACK;
	else
		msg_type = PCU_IF_MSG_START_MEAS_ACK;

	msg = pcu_msgb_alloc(msg_type, 0);
	if (!msg)
		return -ENOMEM;

	pcu_prim = (struct gsm_pcu_if *) msg->data;
	pcu_prim->u.start_meas_req.meas_id = start_meas_req->meas_id;
	pcu_prim->u.start_meas_req.nack_cause = nack_cause;
	pcu_prim->u.start_meas_req.spare[0] = start_meas_req->spare[0];

	LOGP(DL1IF, LOGL_DEBUG, "[PCU->BTS] Sending START MEASurement dump %s (%d)\n", osmo_hexdump(msg->data , msg->data_len), msg->data_len);

	return pcu_sock_send(msg);
}

int pcu_tx_nm_stop_meas_ack_nack(struct gsm_pcu_if_start_stop_meas_req *stop_meas_req, uint8_t nack_cause)
{
	struct msgb *msg;
	uint8_t msg_type;
	struct gsm_pcu_if *pcu_prim;

	if (nack_cause)
		msg_type = PCU_IF_MSG_STOP_MEAS_NACK;
	else
		msg_type = PCU_IF_MSG_STOP_MEAS_ACK;

	msg = pcu_msgb_alloc(msg_type, 0);
	if (!msg)
		return -ENOMEM;

	pcu_prim = (struct gsm_pcu_if *) msg->data;
	pcu_prim->u.stop_meas_req.meas_id = stop_meas_req->meas_id;
	pcu_prim->u.stop_meas_req.nack_cause = nack_cause;

	LOGP(DL1IF, LOGL_DEBUG, "[PCU->BTS] Sending STOP MEASurement dump %s (%d)\n", osmo_hexdump(msg->data , msg->data_len), msg->data_len);

	return pcu_sock_send(msg);
}

int pcu_tx_nm_meas_req_nack(struct gsm_pcu_if_meas_req *meas_req, uint8_t nack_cause)
{
	struct msgb *msg;
	struct gsm_pcu_if *pcu_prim;

	msg = pcu_msgb_alloc(PCU_IF_MSG_MEAS_RES_NACK, 0);
	if (!msg)
		return -ENOMEM;

	pcu_prim = (struct gsm_pcu_if *) msg->data;
	pcu_prim->u.meas_resp.meas_id = meas_req->meas_id;
	pcu_prim->u.meas_resp.nack_cause = nack_cause;

	LOGP(DL1IF, LOGL_DEBUG, "[PCU->BTS] Sending MEASurement RESult REQuest NACK dump %s (%d)\n", osmo_hexdump(msg->data , msg->data_len), msg->data_len);

	return pcu_sock_send(msg);
}

int pcu_tx_nm_meas_resp(struct gsm_pcu_if_meas_resp *meas_resp)
{
	struct msgb *msg;
	struct gsm_pcu_if *pcu_prim;

	msg = pcu_msgb_alloc(PCU_IF_MSG_MEAS_RES_RESP, 0);
	if (!msg)
		return -ENOMEM;

	pcu_prim = (struct gsm_pcu_if *) msg->data;
	pcu_prim->u.meas_resp.meas_id = meas_resp->meas_id;
	pcu_prim->u.meas_resp.nack_cause = 0;
	pcu_prim->u.meas_resp.len = meas_resp->len;
	memcpy(pcu_prim->u.meas_resp.data, meas_resp->data, meas_resp->len);

	LOGP(DL1IF, LOGL_DEBUG, "[PCU->BTS] Sending MEASurement RESult RESPonse dump %s (%d)\n", osmo_hexdump(msg->data , msg->data_len), msg->data_len);

	return pcu_sock_send(msg);
}

static int pcu_rx_nm_start_meas(struct gsm_pcu_if_start_stop_meas_req *start_meas_req)
{
	uint8_t nack_cause = 0;
	struct BTS *bts = BTS::main_bts();

	LOGP(DL1IF, LOGL_INFO, "[BTS->PCU ]START MEASuremnt REQuest received (0x%02x)\n", start_meas_req->meas_id);

	/* measurement ID validation */
	switch(start_meas_req->meas_id) {
	case PCU_IF_IPAC_MEAS_CCCH:
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_DL_ASSIGNMENT);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PDA_POLL_FAILED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_UL_ASSIGNMENT);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PUA_POLL_FAILED);
		break;
	case PCU_IF_IPAC_MEAS_UL_TBF:
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ALLOCATED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_FREED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ABORTED);
		break;
	case PCU_IF_IPAC_MEAS_DL_TBF:
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ALLOCATED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_FREED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ABORTED);
		break;
	case PCU_IF_IPAC_MEAS_LD_TBF:
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX_TIME);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX_TIME);
		break;
	case PCU_IF_IPAC_MEAS_LLC:
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_BYTES);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_PDU_DISC);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_BYTES);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_PDU_DISC);
		break;
	case PCU_IF_IPAC_MEAS_CS_CHG:
	case PCU_IF_IPAC_MEAS_NOMEAS:
		break;
	default:
		nack_cause = NM_NACK_NOTH_REPORT_EXIST;
		break;
	}
	/* send ACK or NACK */
	LOGP(DL1IF, LOGL_INFO, "[PCU->BTS ]START MEASuremnt REQuest %sACK sent\n", nack_cause? "N":"");
	return pcu_tx_nm_start_meas_ack_nack(start_meas_req, nack_cause);
}

static int pcu_rx_nm_stop_meas(struct gsm_pcu_if_start_stop_meas_req *stop_meas_req)
{
	uint8_t nack_cause = 0;
	struct BTS *bts = BTS::main_bts();

	LOGP(DL1IF, LOGL_INFO, "[BTS->PCU ]STOP MEASuremnt REQuest received (0x%02x)\n", stop_meas_req->meas_id);

	/* measurement ID validation */
	switch(stop_meas_req->meas_id) {
	case PCU_IF_IPAC_MEAS_CCCH:
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_DL_ASSIGNMENT);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PDA_POLL_FAILED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_UL_ASSIGNMENT);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PUA_POLL_FAILED);
		break;
	case PCU_IF_IPAC_MEAS_UL_TBF:
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ALLOCATED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_FREED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ABORTED);
		break;
	case PCU_IF_IPAC_MEAS_DL_TBF:
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ALLOCATED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_FREED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ABORTED);
		break;
	case PCU_IF_IPAC_MEAS_LD_TBF:
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX_TIME);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX_TIME);
		break;
	case PCU_IF_IPAC_MEAS_LLC:
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_BYTES);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_PDU_DISC);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_BYTES);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_PDU_DISC);
		break;
	case PCU_IF_IPAC_MEAS_CS_CHG:
	case PCU_IF_IPAC_MEAS_NOMEAS:
		break;
	default:
		nack_cause = NM_NACK_NOTH_REPORT_EXIST;
		break;
	}
	/* send ACK or NACK */
	LOGP(DL1IF, LOGL_INFO, "[PCU->BTS ]STOP MEASuremnt REQuest %sACK sent\n", nack_cause? "N":"");
	return pcu_tx_nm_stop_meas_ack_nack(stop_meas_req, nack_cause);
}


static int pcu_rx_nm_meas_req(struct gsm_pcu_if_meas_req *meas_req)
{
	uint8_t nack_cause = 0;
	struct gsm_pcu_if_meas_resp meas_resp;
	uint32_t val;
	struct BTS *bts = BTS::main_bts();
	uint64_t ul_tbf_att_cnt;
	uint64_t ul_tbf_est_cnt;
	uint64_t ul_tbf_suc_cnt;
	uint64_t dl_tbf_att_cnt;
	uint64_t dl_tbf_est_cnt;
	uint64_t dl_tbf_suc_cnt;

	LOGP(DL1IF, LOGL_INFO, "[BTS->PCU ] MEASuremnt RESult REQuest received (0x%02x)\n", meas_req->meas_id);

	/*FIXME: This is test code for reading rate counters*/
	uint16_t i;
	for (i = 0;i < bts->rate_counters()->desc->num_ctr; i++) {
		LOGP(DPCU, LOGL_DEBUG, "Rate_cnt=%u %s.%u.%s=%"PRIu64"\n",
			i,
			bts->rate_counters()->desc->group_name_prefix,
			bts->rate_counters()->idx,
			bts->rate_counters()->desc->ctr_desc[i].name,
			bts->get_rate_ctr_value(bts->rate_counters(), i));
	}
	/* measurement ID validation */
	switch(meas_req->meas_id) {
	case PCU_IF_IPAC_MEAS_CCCH:
		meas_resp.len = 28;
		/* Packet Downlink Assignments on CCCH */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_DL_ASSIGNMENT);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_PKT_DL_ASS_CNT, val, 0);
		/* Successful Packet Downlink Assignments on CCCH */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_DL_ASSIGNMENT) -
				(uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_PDA_POLL_FAILED);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_SUC_DL_ASS_CNT, val, 7);
		/* Packet Uplink Assignments on CCCH */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_UL_ASSIGNMENT);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_PKT_UL_ASS_CNT, val, 14);
		/* Successful Packet Uplink Assignments on CCCH */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_UL_ASSIGNMENT) -
				(uint32_t)bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_PUA_POLL_FAILED);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_SUC_UL_ASS_CNT, val, 21);
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_DL_ASSIGNMENT);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PDA_POLL_FAILED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PKT_UL_ASSIGNMENT);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_PUA_POLL_FAILED);
		break;
	case PCU_IF_IPAC_MEAS_UL_TBF:
		meas_resp.len = 21;
		ul_tbf_att_cnt = bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ALLOCATED);
		ul_tbf_est_cnt = bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_FREED);
		ul_tbf_suc_cnt = bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_FREED) -
							bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ABORTED);
		/* Attempted Uplink TBFs */
		val = (uint32_t) ul_tbf_att_cnt;
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_ATT_UL_TBF_CNT, val, 0);
		/* Established uplink TBFs */
		val = (uint32_t) ul_tbf_est_cnt;
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_EST_UL_TBF_CNT, val, 7);
		/* Successful uplink TBFs */
		val = (uint32_t) ul_tbf_suc_cnt;
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_SUC_UL_TBF_CNT, val, 14);
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ALLOCATED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_FREED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_ABORTED);
		break;
	case PCU_IF_IPAC_MEAS_DL_TBF:
		meas_resp.len = 21;
		dl_tbf_att_cnt = bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ALLOCATED);
		dl_tbf_est_cnt = bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_FREED);
		dl_tbf_suc_cnt = bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_FREED) -
							bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ABORTED);
		/* Attempted Downlink TBFs */
		val = (uint32_t) dl_tbf_att_cnt;
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_ATT_DL_TBF_CNT, val, 0);
		/* Established downlink TBFs */
		val = (uint32_t) dl_tbf_est_cnt;
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_EST_DL_TBF_CNT, val, 7);
		/* Successful downlink TBFs */
		val = (uint32_t) dl_tbf_suc_cnt;
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_SUC_DL_TBF_CNT, val, 14);
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ALLOCATED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_FREED);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_ABORTED);
		break;
	case PCU_IF_IPAC_MEAS_LD_TBF:
		meas_resp.len = 28;
		/* Maximum downlink TBFs */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_MAX_DL_TBF_CNT, val, 0);
		/* Maximum uplink TBFs */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_MAX_UL_TBF_CNT, val, 7);
		/* Time at maximum downlink TBFs */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX_TIME);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_TMAX_DL_TBF_CNT, val, 14);
		/* Time at maximum uplink TBFs */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX_TIME);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_TMAX_UL_TBF_CNT, val, 21);
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_DL_MAX_TIME);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_TBF_UL_MAX_TIME);
		break;
	case PCU_IF_IPAC_MEAS_LLC:
		meas_resp.len = 28;
		/* Downlink LLC Octets */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_BYTES);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_DL_LLC_OCT_CNT, val, 0);
		/* Uplink LLC Octets */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_BYTES);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_UL_LLC_OCT_CNT, val, 7);
		/* Downlink LLC PDU Discarded */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_PDU_DISC);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_DL_LLC_PDUD_CNT, val, 14);
		/* Uplink LLC PDU Discarded */
		val = (uint32_t) bts->get_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_PDU_DISC);
		PCU_PACK_PCOUNTER(meas_resp.data, PCU_IF_IPAC_UL_LLC_PDUD_CNT, val, 21);
		/* Reset related counters for this group */
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_BYTES);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_DL_PDU_DISC);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_BYTES);
		bts->reset_rate_ctr_value(bts->rate_counters(), bts->CTR_LLC_UL_PDU_DISC);
		break;
	case PCU_IF_IPAC_MEAS_CS_CHG:
		meas_resp.len = 0;
		break;
	default:
		LOGP(DL1IF, LOGL_INFO, "[BTS->PCU ] Invalid MEASuremnt RESult REQuest received (0x%02x)\n", meas_req->meas_id);
		/* send NACK */
		nack_cause = NM_NACK_NOTH_REPORT_EXIST;
		return pcu_tx_nm_meas_req_nack(meas_req, nack_cause);
	}

	meas_resp.meas_id = meas_req->meas_id;

	return pcu_tx_nm_meas_resp(&meas_resp);
}

static int pcu_rx_susp_req(struct gsm_pcu_if_susp_req *susp_req)
{
	struct bssgp_bvc_ctx *bctx = gprs_bssgp_pcu_current_bctx();
	struct gprs_ra_id ra_id;

	gsm48_parse_ra(&ra_id, susp_req->ra_id);

	LOGP(DL1IF, LOGL_DEBUG, "GPRS Suspent request received: tlli=0x%08x\n", susp_req->tlli);

	if (!bctx)
		return -1;

	/* TODO: forward to SGSN with proper information of given MS */
	return 0;
}

int pcu_rx(uint8_t msg_type, struct gsm_pcu_if *pcu_prim)
{
	int rc = 0;
	struct gprs_rlcmac_bts *bts = bts_main_data();
	struct pcu_alarm_list *alarm_list;

	/* check for the PCU alarm has already sent or not */
	llist_for_each_entry(alarm_list, &bts->alarm_list, list) {
		if (alarm_list->alarm_signal != S_PCU_NM_WRONG_IF_VER_ALARM)
			continue;
		llist_del(&alarm_list->list);
		LOGP(DL1IF, LOGL_DEBUG, "Alarm 0x%04x has removed from PCU sent alarm list\n", alarm_list->alarm_signal);
		exit(-1);
	}

	switch (msg_type) {
	case PCU_IF_MSG_DATA_IND:
		rc = pcu_rx_data_ind(&pcu_prim->u.data_ind);
		break;
	case PCU_IF_MSG_DATA_CNF:
		rc = pcu_rx_data_cnf(&pcu_prim->u.data_cnf);
		break;
	case PCU_IF_MSG_RTS_REQ:
		rc = pcu_rx_rts_req(&pcu_prim->u.rts_req);
		break;
	case PCU_IF_MSG_RACH_IND:
		rc = pcu_rx_rach_ind(&pcu_prim->u.rach_ind);
		break;
	case PCU_IF_MSG_INFO_IND:
		rc = pcu_rx_info_ind(&pcu_prim->u.info_ind);
		break;
	case PCU_IF_MSG_TIME_IND:
		rc = pcu_rx_time_ind(&pcu_prim->u.time_ind);
		break;
	case PCU_IF_MSG_PAG_REQ:
		rc = pcu_rx_pag_req(&pcu_prim->u.pag_req);
		break;
	case PCU_IF_MSG_START_MEAS_REQ:
		rc = pcu_rx_nm_start_meas(&pcu_prim->u.start_meas_req);
		break;
	case PCU_IF_MSG_MEAS_RES_REQ:
		rc = pcu_rx_nm_meas_req(&pcu_prim->u.meas_req);
		break;
	case PCU_IF_MSG_STOP_MEAS_REQ:
		rc = pcu_rx_nm_stop_meas(&pcu_prim->u.stop_meas_req);
		break;
	case PCU_IF_MSG_SUSP_REQ:
		rc = pcu_rx_susp_req(&pcu_prim->u.susp_req);
		break;
	default:
		LOGP(DL1IF, LOGL_ERROR, "Received unknwon PCU msg type %d\n",
			msg_type);
		alarm_sig_data.spare[0] = msg_type;
		osmo_signal_dispatch(SS_L_GLOBAL, S_PCU_NM_RX_UNKN_MSG_ALARM, &alarm_sig_data);
		rc = -EINVAL;
	}
	return rc;
}

static int handle_pcu_fail_evt_rep_sig(unsigned int subsys, unsigned int signal,
			void *handler_data, void *_signal_data)
{
	struct pcu_fail_evt_rep_sig_data *sig_data = (struct pcu_fail_evt_rep_sig_data *)_signal_data;
	int rc = 0;
	unsigned int res;
	char log_msg[100];
	uint16_t val;

	if (subsys != SS_L_GLOBAL)
		return 0;

	switch (signal) {
	case S_PCU_NM_WRONG_IF_VER_ALARM:
		memcpy(&res, sig_data->spare, sizeof(unsigned int));
		snprintf(log_msg, 100, "PCU interface version number of BTS (%d) is different (%d)", res, PCU_IF_VERSION);
		sig_data->add_text = &log_msg[0];

		rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
				NM_SEVER_CRITICAL,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_CRIT_BAD_PCU_IF_VER,
				sig_data->add_text);
		break;
#ifdef ENABLE_DIRECT_PHY
	case S_PCU_NM_FAIL_OPEN_PDCH_ALARM:
		snprintf(log_msg, 100, "PCU: Failed to open direct DSP (%d) access for PDCH.\n", sig_data->spare[0]);
		sig_data->add_text = &log_msg[0];

		rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
				NM_SEVER_CRITICAL,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_CRIT_OPEN_PDCH_FAIL,
				sig_data->add_text);
		break;

	case S_PCU_NM_FAIL_OPEN_L1_ALARM:
		snprintf(log_msg, 100, "PCU: Failed to open L1 transport for TRX %d", sig_data->spare[0]);
		sig_data->add_text = &log_msg[0];

		rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
				NM_SEVER_CRITICAL,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_CRIT_OPEN_L1_FAIL,
				sig_data->add_text);
		break;

	case S_PCU_NM_RX_UNKN_L1_SAP_ALARM:
			memcpy(&res, sig_data->spare, sizeof(unsigned int));
			snprintf(log_msg, 100, "PCU: Rx PH-DATA.ind for unknown L1 SAPI 0x%x\n", res);
			sig_data->add_text = &log_msg[0];

			rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
					NM_SEVER_MAJOR,
					NM_PCAUSE_T_MANUF,
					PCU_NM_EVT_CAUSE_MAJ_UKWN_L1_MSG,
					sig_data->add_text);
			break;

	case S_PCU_NM_RX_UNKN_L1_PRIM_ALARM:
				memcpy(&res, sig_data->spare, sizeof(unsigned int));
				snprintf(log_msg, 100, "PCU: Rx unknown L1 primitive type 0x%x\n", res);
				sig_data->add_text = &log_msg[0];

				rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
						NM_SEVER_MAJOR,
						NM_PCAUSE_T_MANUF,
						PCU_NM_EVT_CAUSE_MAJ_UKWN_L1_PRIM_MSG,
						sig_data->add_text);
				break;

	case S_PCU_NM_PDTCH_QUEUE_FULL_ALARM:
		rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
				NM_SEVER_MAJOR,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_MAJ_PDTCH_QUEUE_FULL,
				"PCU: PDTCH queue full. PCU is dropping message.\n");
		break;

#endif
	case S_PCU_NM_NO_PDCH_ALARM:
		rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
				NM_SEVER_WARNING,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_WARN_NO_PDCH_AVAIL,
				"PCU no PDCH available\n");
		break;

	case S_PCU_NM_RX_UNKN_MSG_ALARM:
		snprintf(log_msg, 100, "PCU: Rx unknown BTS message type 0x%x\n", sig_data->spare[0]);
		sig_data->add_text = &log_msg[0];

		rc = pcu_tx_nm_fail_evt(NM_EVT_PROC_FAIL,
				NM_SEVER_MAJOR,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_MAJ_UKWN_BTS_MSG,
				sig_data->add_text);
		break;

	case S_PCU_NM_FAIL_NSVC_ALARM:
			memcpy(&val, sig_data->spare, sizeof(uint16_t));
			snprintf(log_msg, 100, "PCU: NS-VC %d failure\n", val);
			sig_data->add_text = &log_msg[0];

			rc = pcu_tx_nm_fail_evt(NM_EVT_COMM_FAIL,
					NM_SEVER_MAJOR,
					NM_PCAUSE_T_MANUF,
					PCU_NM_EVT_CAUSE_CRIT_NSVC_FAIL,
					sig_data->add_text);
			break;

	case S_PCU_NM_FAIL_RST_NSVC_ALARM:
		memcpy(&val, sig_data->spare, sizeof(uint16_t));
		snprintf(log_msg, 100, "PCU: NS-VC %d reset failure\n", val);
		sig_data->add_text = &log_msg[0];
		rc = pcu_tx_nm_fail_evt(NM_EVT_COMM_FAIL,
				NM_SEVER_MAJOR,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_CRIT_NSVC_RST_FAIL,
				sig_data->add_text);
		break;

	case S_PCU_NM_FAIL_UNBLK_NSVC_ALARM:
		memcpy(&val, sig_data->spare, sizeof(uint16_t));
		snprintf(log_msg, 100, "PCU: NS-VC %d unblock failure\n", val);
		sig_data->add_text = &log_msg[0];
		rc = pcu_tx_nm_fail_evt(NM_EVT_COMM_FAIL,
				NM_SEVER_MAJOR,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_CRIT_NSVC_UNBLK_FAIL,
				sig_data->add_text);
		break;

	case S_PCU_NM_FAIL_PTP_BVC_ALARM:
			memcpy(&val, sig_data->spare, sizeof(uint16_t));
			snprintf(log_msg, 100, "PCU:BVCI %d PTP failure\n", val);
			sig_data->add_text = &log_msg[0];
			rc = pcu_tx_nm_fail_evt(NM_EVT_COMM_FAIL,
					NM_SEVER_MAJOR,
					NM_PCAUSE_T_MANUF,
					PCU_NM_EVT_CAUSE_MAJ_PTP_BVC_FAIL,
					sig_data->add_text);
			break;

	case S_PCU_NM_UNKN_NSEI_BVCI_ALARM:
		memcpy(&val, sig_data->spare, sizeof(uint16_t));
		snprintf(log_msg, 100, "PCU: Rx unknown BVCI %d\n", val);
		sig_data->add_text = &log_msg[0];
		rc = pcu_tx_nm_fail_evt(NM_EVT_COMM_FAIL,
				NM_SEVER_MAJOR,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_MAJ_UNKN_NSEI_BVCI,
				sig_data->add_text);
		break;

	case S_PCU_NM_UNKN_NSVC_ALARM:
		memcpy(&val, sig_data->spare, sizeof(uint16_t));
		snprintf(log_msg, 100, "PCU: Rx unknown NSVC %d\n", val);
		sig_data->add_text = &log_msg[0];
		rc = pcu_tx_nm_fail_evt(NM_EVT_COMM_FAIL,
				NM_SEVER_MAJOR,
				NM_PCAUSE_T_MANUF,
				PCU_NM_EVT_CAUSE_MAJ_UNKN_NSVC,
				sig_data->add_text);
		break;

	default:
		break;
	}

	sig_data->rc = rc;
	return rc;
}

/* Initialization of the PCU failure alarm event handler */
void pcu_failure_report_init(void *handler)
{
	/* register for failure report events */
	osmo_signal_register_handler(SS_L_GLOBAL, handle_pcu_fail_evt_rep_sig, handler);
}

/* Uninitizalization of the PCU failure alarm event handler */
void pcu_failure_report_free(void *handler)
{
	/* unregister for failure report events */
	osmo_signal_unregister_handler(SS_L_GLOBAL, handle_pcu_fail_evt_rep_sig, handler);
}

/* externally linkage wrapper for L1 PDCH ACT */
int rlcmac_pdch_act_req (uint8_t trx, uint8_t ts, uint8_t activate)
{
	return pcu_tx_act_req(trx, ts, activate);
}
